Object.prototype[Symbol.toPrimitive] = function(hint) {
  let primitive = this;

  function isPrimitive(val) {
    return val !== Object(val);
  }

  if(isPrimitive(primitive)) {
    return primitive;
  }
  
  if(hint === 'string' ) {
    console.log('Conversion type: ' + hint);
    primitive = this.toString();
    if(isPrimitive(primitive)) {
      return primitive;
    }
    primitive = this.valueOf()
    if(isPrimitive(primitive)) {
      return primitive;
    }
    throw TypeError('Cannot convert object to primitive value');
  }

  console.log('Conversion type: ' + hint);
  primitive = this.valueOf();
  if(isPrimitive(primitive)) {
    return primitive;
  }
  primitive = this.toString()
  if(isPrimitive(primitive)) {
    return primitive;
  }
  throw TypeError('Cannot convert object to primitive value');
}

let a = {
  number: 23,
  // valueOf() {
  //   return this.number;
  // }
  // toString() {
  //   return {};
  // }
};
let b = 10;
let c = String('str');

// String
// alert(a);
console.log(String([1]));
console.log(`${a}`);

// Number
console.log(+a);
console.log(Number(a));
console.log(a / b);

// Default
console.log(a + b);
console.log(a == b);
console.log(a == 'string');
